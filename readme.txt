=== Plugin Name ===
Contributors: artbelov
Tags: currency, ruble, exchange, USD, EUR, RUB
Requires at least: 3.3
Tested up to: 4.9
Requires PHP: 5.6
Stable tag: 0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
The plugin adds the widget which displays exchange rates for US Dollar and Euro to Russian ruble according to 
The Russian Central Bank.
In addition, it displays the chart with weekly data about the the exchange rates.

The plugin utilizes [XML API](http://www.cbr.ru/development/sxml/) by The Russian Central Bank in order to retrieve the currency data.

Critics and suggestions are welcome.
 
== Installation ==
 
1. Copy the plugin directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
 
== Screenshots ==
 
1. Basic look  
2. Basic look without the chart
3. Widget settings interface
 
== Changelog ==
 
= 0.6 =
* Initial release
